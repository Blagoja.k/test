<?php

namespace App\Http\Controllers;


use \Illuminate\Http\Request;

class MoviesController extends Controller
{

	public function showMovies()
	{

		return view('show-movies');
	}

	public function showDetails(Request $request)
	{
		$ime = $request->get('title');
		$godina = $request->get('godina');
		$reziser = $request->get('director');
		$akter - $request->get('actor');

		return view('show-details', [

				'Ime' => $ime,
				'Year' => $godina,
				'Director' => $reziser,
				'Actor' => $akter
			]);
		}
	}
	
